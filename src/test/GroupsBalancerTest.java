package test;

import core.logic.GroupsBalancer;
import core.math.Dispersion;
import core.models.Group;
import core.models.Team;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class GroupsBalancerTest {

    public static GroupsBalancer groupsBalancer;
    public static List<Team> teams;

    public static Team brasil;
    public static Team bolivia;
    public static Team venezuela;
    public static Team peru;
    public static Team argentina;
    public static Team colombia;
    public static Team paraguay;
    public static Team qatar;
    public static Team uruguay;
    public static Team ecuador;
    public static Team japon;
    public static Team chile;

    public static Group groupA;
    public static Group groupB;
    public static Group groupC;

    @BeforeAll
    public static void setup() {
        groupsBalancer = new GroupsBalancer();

        brasil = new Team("brasil", 1);
        bolivia = new Team("bolivia", 1);
        venezuela = new Team("venezuela", 1);
        peru = new Team("peru", 1);
        argentina = new Team("argentina", 22);
        colombia = new Team("colombia", 24);
        paraguay = new Team("paraguay", 26);
        qatar = new Team("qatar", 28);
        uruguay = new Team("uruguay", 100);
        ecuador = new Team("ecuador", 110);
        japon = new Team("japon", 120);
        chile = new Team("chile", 130);

        teams = new ArrayList<Team>();
        teams.add(brasil);
        teams.add(bolivia);
        teams.add(venezuela);
        teams.add(peru);

        teams.add(argentina);
        teams.add(colombia);
        teams.add(paraguay);
        teams.add(qatar);

        teams.add(uruguay);
        teams.add(ecuador);
        teams.add(japon);
        teams.add(chile);

        groupA = new Group();
        groupA.addTeam(brasil);
        groupA.addTeam(bolivia);
        groupA.addTeam(venezuela);
        groupA.addTeam(peru);


        groupB = new Group();
        groupB.addTeam(argentina);
        groupB.addTeam(colombia);
        groupB.addTeam(paraguay);
        groupB.addTeam(qatar);


        groupC = new Group();
        groupC.addTeam(uruguay);
        groupC.addTeam(ecuador);
        groupC.addTeam(japon);
        groupC.addTeam(chile);

    }

    @Test
    @Ignore
    public void balancedTeams() {
        List<Group> expected = new ArrayList<Group>();
        expected.add(groupA);
        expected.add(groupB);
        expected.add(groupC);

//        List<Group> balancedGroups = groupsBalancer.balance(teams);

//        assertTrue(balancedGroups.size() == 3);
//        assertTrue(Dispersion.calculate(balancedGroups.get(0).getTeamForces()) <= Dispersion.calculate(balancedGroups.get(0).getTeamForces()));
//        assertTrue(Dispersion.calculate(balancedGroups.get(1).getTeamForces()) <= Dispersion.calculate(balancedGroups.get(2).getTeamForces()));
    }
}
