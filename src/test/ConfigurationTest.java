package test;

import core.models.Configuration;
import core.models.Group;
import core.models.Team;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConfigurationTest {

    private static Configuration configuration;

    @BeforeAll
    public static void setup() {
        configuration = new Configuration();

        Team t1 = new Team("Argentina", 1582);
        Team t2 = new Team("Bolivia", 1365);
        Team t3 = new Team("Brasil", 1681);
        Team t4 = new Team("Chile", 1561);
        Team t5 = new Team("Colombia", 1580);
        Team t6 = new Team("Ecuador", 1375);
        Team t7 = new Team("Paraguay", 1468);
        Team t8 = new Team("Peru", 1516);

        Group g1 = new Group();
        g1.addTeam(t1);
        g1.addTeam(t2);
        g1.addTeam(t3);
        g1.addTeam(t4);

        Group g2 = new Group();
        g2.addTeam(t5);
        g2.addTeam(t6);
        g2.addTeam(t7);
        g2.addTeam(t8);

        configuration.addGroup(g1);
        configuration.addGroup(g2);
    }

    @Test
    public void noContieneEquiposRepetidos() {
        assertTrue(configuration.isValid());
    }

    @Test
    public void contieneEquiposRepetidos() {
        Team t = configuration.getGroups().get(0).getTeams().get(0);
        configuration.getGroups().get(0).getTeams().remove(1);
        configuration.getGroups().get(0).addTeam(t);

        assertFalse(configuration.isValid());
    }
}
