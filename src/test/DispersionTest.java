package test;


import core.math.Dispersion;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DispersionTest {

    private static List<Double> zeroDispersion;
    private static List<Double> hasDispersion;

    @BeforeAll
    public static void setupOne() {
        zeroDispersion = new ArrayList<Double>();
        zeroDispersion.add(1.0);
        zeroDispersion.add(1.0);
        zeroDispersion.add(1.0);
        zeroDispersion.add(1.0);

        hasDispersion = new ArrayList<Double>();
        hasDispersion.add(1.0);
        hasDispersion.add(1.0);
        hasDispersion.add(1.0);
        hasDispersion.add(2.0);
    }

    @Test
    @DisplayName("Valores sin dispersion")
    public void dispersionTest() {
        double expected = 0.0;
        double actual = Dispersion.calculate(zeroDispersion);

        assertTrue(actual == expected);
    }

    @Test
    @DisplayName("Valores con dispersion = 0.25")
    public void hasDispersionTest() {
        double expected = 0.25;
        double actual = Dispersion.calculate(hasDispersion);

        assertTrue(actual == expected);
    }

    @Test
    @DisplayName("Void list")
    public void voidListTest() {
        assertThrows(IllegalArgumentException.class, () -> {
           double dispersion = Dispersion.calculate(new ArrayList<Double>());
        });
    }
}
