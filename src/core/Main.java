package core;

import core.gui.MainFrame;
import core.models.Team;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        new MainFrame().run();

        List<Team> teams = new ArrayList<Team>();
        teams.add(new Team("Argentina", 1582));
        teams.add(new Team("Bolivia", 1365));
        teams.add(new Team("Brasil", 1681));
        teams.add(new Team("Chile", 1561));
        teams.add(new Team("Colombia", 1580));
        teams.add(new Team("Ecuador", 1375));
        teams.add(new Team("Paraguay", 1468));
        teams.add(new Team("Peru", 1516));
        teams.add(new Team("Japon", 1369));
        teams.add(new Team("Qatar", 1112));
        teams.add(new Team("Uruguay", 1616));
        teams.add(new Team("Venezuela", 1430));

    }
}
