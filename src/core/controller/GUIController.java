package core.controller;

import core.logic.GroupsBalancer;
import core.models.Configuration;
import core.models.Team;

import java.util.List;

public class GUIController {

    private static GUIController ourInstance = new GUIController();

    public static GUIController getInstance() {
        return ourInstance;
    }

    private GroupsBalancer groupsBalancer;

    private GUIController() {
        this.groupsBalancer =  new GroupsBalancer();
    }

    public Configuration balanceTeams(List<Team> teams) {
        return this.groupsBalancer.balance(teams);
    }

    public int getConfigurationsTested() {
        return this.groupsBalancer.getConfigurationsSize();
    }
}
