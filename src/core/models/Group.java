package core.models;

import core.math.Dispersion;

import java.util.ArrayList;
import java.util.List;

public class Group {

    private List<Team> teams;

    public Group() {
        this.teams = new ArrayList<Team>();
    }

    public Group(Group group) {
        this.teams = new ArrayList<Team>(group.getTeams());
    }

    public Group(List<Team> teams) {
        this.teams = teams;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public void addTeam(Team team) {
        this.teams.add(team);
    }

    public void removeTeam(Team team) {
        this.teams.remove(team);
    }

    public List<Double> getTeamForces() {
        List<Double> teamForces = new ArrayList<Double>();

        for(Team team : this.teams) {
            teamForces.add(team.getForce());
        }

        return teamForces;
    }

    public boolean equals(Group other) {
        return this.getTeams().stream().allMatch(o -> other.contains(o));
    }

    public boolean contains(Team other) {
        return this.getTeams().stream().anyMatch(o -> o.equals(other));
    }

    public int size() {
        return this.getTeams().size();
    }

    public double getDispersion() {
        return Dispersion.calculate(this.getTeamForces());
    }
}
