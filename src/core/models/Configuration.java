package core.models;

import java.util.ArrayList;
import java.util.List;

public class Configuration {
    private List<Group> groups;

    public Configuration() {
        this.groups = new ArrayList<>();
    }

    public Configuration(Configuration configuration) {
        this.groups = new ArrayList<>(configuration.getGroups());
    }

    public Configuration(List<Group> groups) {
        this.groups = groups;
    }

    public List<Group> getGroups() {
        return this.groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public void addGroup(Group group) {
        if(this.groups == null)
            this.groups = new ArrayList<>();

        this.groups.add(group);
    }

    public void removeGroup(Group group) {
        if(this.groups == null)
            this.groups = new ArrayList<>();

        this.groups.remove(group);
    }

    public boolean contains(Group group) {
        return this.getGroups().stream().anyMatch(o -> o.equals(group));
    }

    /**
     * Una configuracion es valida si y solo si la cantidad de equipos que contiene es igual a la cantidad de equipos unicos.
     * Es decir que no puede contener equipos repetidos.
     *
     * @return {@literal true} si la configuracion es valida. {@literal false} en caso contrario
     */
    public boolean isValid() {
        int expectedTeams = countTeams();
        Group group = new Group();

        for(Group g : this.getGroups()) {
            for(Team t : g.getTeams()) {
                if(!group.contains(t)) {
                    group.addTeam(t);
                }
            }
        }

        return group.size() == expectedTeams;
    }

    /**
     * Devuelve la cantidad de equipos que hay en esta configuracion
     *
     * @return int cantidad de equipos en la configuracion
     */
    private int countTeams() {
        int teams = 0;
        for(Group group : this.getGroups()) {
            teams += group.getTeams().size();
        }

        return teams;
    }

    public double getAverageDispersion() {
        double dispersions = 0.0;
        for(Group g : this.getGroups()) {
            dispersions += g.getDispersion();
        }

        return dispersions / this.getGroups().size();
    }

    public double getMinDispersion() {
        double min = Double.POSITIVE_INFINITY;
        for(Group g : this.getGroups()) {
            double dispersion = g.getDispersion();
            if(dispersion < min) {
                min = dispersion;
            }
        }
        return min;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        for(Group group : this.getGroups()) {
            sb.append("\t{\n");
            for(Team t : group.getTeams()) {
                sb.append("\t\t" + t);
            }
            sb.append("\t}\n");
        }
        sb.append("}\n");

        return sb.toString();
    }

    public int size() {
        return this.getGroups().size();
    }
}
