package core.models;

public class Team {

    private String name;

    private double force;

    public Team(String name, double force) {
        this.name = name;
        this.force = force;
    }

    public Team(Team t) {
        this.name = t.getName();
        this.force = t.getForce();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getForce() {
        return force;
    }

    public void setForce(double force) {
        this.force = force;
    }

    /**
     * Un equipo es igual a otro si y solo si tienen el mismo nombre y la misma fuerza
     *
     * @param other otro equipo para comparar
     *
     * @return {@literal true} si ambos equipos tienen el mismo nombre y la misma fuerza. {@literal false} caso contrario
     */
    public boolean equals(Team other) {
        return this.getName().equals(other.getName()) && this.getForce() == other.getForce();
    }

    @Override
    public String toString() {
        return "{ name: " + this.getName() + ", force: " + this.getForce() + "}\n";
    }
}
