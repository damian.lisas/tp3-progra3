package core.gui;

import core.controller.GUIController;
import core.models.Team;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame {

    private JFrame mainFrame;
    private TeamPanel teamsPanel;
    private ConfigurationPanel configPanel;
    private int width;
    private int height;

    public MainFrame() {
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        this.width = gd.getDisplayMode().getWidth();
        this.height = gd.getDisplayMode().getHeight();

        this.mainFrame = new JFrame();
        this.mainFrame.setBounds(0, 0, this.width, this.height);
        this.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.mainFrame.getContentPane().setLayout(null);

        this.teamsPanel = new TeamPanel();
        this.teamsPanel.setBounds(0, 0, 150, 60);

        this.mainFrame.getContentPane().add(teamsPanel);

        JButton addTeamBtn = new JButton("Agregar equipo");
        addTeamBtn.setBounds(220,0,200, 20);
        addTeamBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(teamsPanel.teamsAmmount() <= 12) {
                    String nombre = JOptionPane.showInputDialog("Ingrese el nombre del equipo (Al menos 3 caracteres)");
                    Double fuerza = 0.0;
                    try {
                         fuerza = Double.valueOf(JOptionPane.showInputDialog("Ingrese el nombre del equipo"));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(mainFrame, "El numero ingresado es invalido");
                    }
                    if(nombre != null && nombre.length() >= 3 && fuerza != null){
                        teamsPanel.addTeam(new Team(nombre, fuerza));
                    } else {
                        JOptionPane.showMessageDialog(mainFrame, "El equipo ingresado no es valido");
                    }
                } else {
                    JOptionPane.showMessageDialog(mainFrame, "Solo pueden ingresarse 12 equipos!");
                }
            }
        });
        this.mainFrame.getContentPane().add(addTeamBtn);

        JButton testTeamsBtn = new JButton("Usar equipos de prueba\n(Recomendado)");
        testTeamsBtn.setBounds(220, 25, 200, 20);
        testTeamsBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(teamsPanel.teamsAmmount() == 0){
                    teamsPanel.addTeam(new Team("Argentina", 1582));
                    teamsPanel.addTeam(new Team("Bolivia", 1365));
                    teamsPanel.addTeam(new Team("Brasil", 1681));
                    teamsPanel.addTeam(new Team("Chile", 1561));
                    teamsPanel.addTeam(new Team("Colombia", 1580));
                    teamsPanel.addTeam(new Team("Ecuador", 1375));
                    teamsPanel.addTeam(new Team("Paraguay", 1468));
                    teamsPanel.addTeam(new Team("Peru", 1516));
                    teamsPanel.addTeam(new Team("Japon", 1369));
                    teamsPanel.addTeam(new Team("Qatar", 1112));
                    teamsPanel.addTeam(new Team("Uruguay", 1616));
                    teamsPanel.addTeam(new Team("Venezuela", 1430));
                } else {
                    JOptionPane.showMessageDialog(mainFrame, "Este boton agrega los 12 equipos, elimine todos los equipos primero.");
                }
            }
        });
        this.mainFrame.getContentPane().add(testTeamsBtn);

        this.configPanel = new ConfigurationPanel();
        this.mainFrame.getContentPane().add(configPanel);

        JButton generarConfiguracionesBtn = new JButton("Generar configuracion");
        generarConfiguracionesBtn.setBounds(220, 50, 200, 20);
        generarConfiguracionesBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(teamsPanel.teamsAmmount() == 12) {
                    JOptionPane.showMessageDialog(mainFrame, "Esto va a tardar un rato...");
                    configPanel.addConfiguration(GUIController.getInstance().balanceTeams(teamsPanel.getTeams()));
                    JOptionPane.showMessageDialog(mainFrame, "Configuracion completada! Se evaluaron " + GUIController.getInstance().getConfigurationsTested() + " configuraciones");
                } else {
                    JOptionPane.showMessageDialog(mainFrame, "Debe haber 12 equipos para poder generar una configuracion");
                }
            }
        });
        this.mainFrame.getContentPane().add(generarConfiguracionesBtn);
    }

    public void run()
    {
        try {
            MainFrame window = new MainFrame();
            window.mainFrame.setVisible(true);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
