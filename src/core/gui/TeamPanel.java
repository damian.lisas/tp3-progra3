package core.gui;

import core.models.Team;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class TeamPanel extends JPanel {

    private List<Team> teams;
    private GridLayout layout;

    public TeamPanel() {
        this.teams = new ArrayList<>();

        this.layout = newLayout();

        super.setBackground(Color.gray);

        super.add(new Label("Pais"));
        super.add(new Label("Fuerza"));
    }

    private GridLayout newLayout() {
        GridLayout gl = new GridLayout();
        gl.setColumns(2);
        gl.setRows(this.teams.size() + 1);
        return gl;
    }

    public void addTeam(Team team) {
        this.teams.add(team);
        addUIComponent(team);
    }

    private void addUIComponent(Team team) {
        this.layout.setRows(this.layout.getRows() + 1);
        this.setBounds(this.getX(), this.getY(), this.getWidth(), this.getHeight() + 30);
        super.add(new Label(team.getName()));
        super.add(new Label(String.valueOf(team.getForce())));
        super.updateUI();
    }

    public int teamsAmmount() {
        return this.teams.size();
    }

    public List<Team> getTeams() {
        return this.teams;
    }

//    public void removeTeam(Team team) {
//        this.teams.remove(team);
//    }
}
