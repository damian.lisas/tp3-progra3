package core.gui;

import core.models.Configuration;

import javax.swing.*;
import java.awt.*;

public class ConfigurationPanel extends JPanel {

    private Configuration configuration;

    public ConfigurationPanel() {
        this.configuration = new Configuration();
        super.setBounds(0, 600, 800, 100);
        super.setBackground(Color.gray);
    }

    public void addConfiguration(Configuration configuration) {
        for(int i = 0; i < configuration.getGroups().size(); i++) {
            JTextPane jtp = new JTextPane();
            jtp.setBackground(Color.lightGray);
            jtp.setBounds(100 * i, 600, 200, 100);
            jtp.setText("Grupo " + (i + 1) + ": " + configuration.getGroups().get(i).getTeams());
            this.add(jtp);
            super.updateUI();
        }
    }
}
