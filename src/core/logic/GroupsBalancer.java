package core.logic;

import core.models.Configuration;
import core.models.Group;
import core.models.Team;

import java.util.ArrayList;
import java.util.List;

public class GroupsBalancer {

    public final int GROUPS_SIZE = 4;
    public final int CONFIGURATIONS_SIZE = 3;

    private List<Team> inputTeams;
    private List<Group> groups;
    private List<Configuration> configurations;
    private Configuration best;

    public GroupsBalancer() {
        this.groups = new ArrayList<>();
        this.configurations = new ArrayList<>();
        this.best = null;
    }

    /**
     * Recibe una lista de grupos y los balancea en funcion de la fuerza de los equipos para que sean mas parejos
     * Los grupos son generados de a 4 por lo que la cantidad de equipos que se pasen tienen que ser multiplo de 4
     *
     * @param equipos Grupos a balancear
     * @return grupos balanceados
     */
    public Configuration balance(List<Team> equipos) {

        if(equipos.size() % GROUPS_SIZE != 0) {
            throw new IllegalArgumentException("La cantidad de equipos debe ser multiplo de " + GROUPS_SIZE + ". " + equipos.size());
        }

        this.inputTeams = equipos;

        System.out.println("Definiendo grupos posibles");
        generateGroups(new Group());
        System.out.println("Grupos generados = " + this.groups.size());

        System.out.println("Definiendo configuraciones posibles");
        defineConfigurations(new Configuration());
        System.out.println("Configuraciones posibles = " + this.configurations.size());

        return best();
    }

    private void generateGroups(Group group) {

        if(group.getTeams().size() == GROUPS_SIZE) {
            if(this.groups.stream().allMatch(o -> !o.equals(group)))
                this.groups.add(new Group(group));
            return;
        }

        for(Team team : this.inputTeams) {
            if(!group.contains(team)) {
                Team clon = new Team(team);
                group.addTeam(clon);
                generateGroups(group);
                group.removeTeam(clon);
            }
        }
    }

    private void defineConfigurations(Configuration actual) {
        //caso base
        if(actual.size() == CONFIGURATIONS_SIZE) {
            if(this.configurations.size() % 34649 == 0)
                System.out.println("Holi");
            this.configurations.add(new Configuration(actual));
            if(this.best == null){
                this.best = new Configuration(actual);
            } else if (actual.getAverageDispersion() > this.best.getAverageDispersion() && actual.getMinDispersion() > this.best.getMinDispersion()){
//                if(actual.getGroups().size() == 0)
//                    System.out.println("Oli");
                this.best = new Configuration(actual);
            }
            return;
        }

        for(Group g : this.groups) {
            if(!actual.contains(g)) {
                Group clon = new Group(g);
                actual.addGroup(clon);
                if(actual.isValid()) {
                    defineConfigurations(new Configuration(actual));
                }
                actual.removeGroup(clon);
            }
        }

    }

    private Configuration best() {
        return this.best;
    }

    public int getConfigurationsSize() {
        return this.configurations.size();
    }
}
