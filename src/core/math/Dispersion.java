package core.math;

import java.util.List;

public class Dispersion {

    public static double calculate(List<Double> G) {
        if(G == null || G.isEmpty())
            throw new IllegalArgumentException("Can't calculate the dispersion of an empty List");

        double average = calculateAverage(G);
        double dispersion = calculateDispersion(G, average);
        return dispersion;
    }

    private static double calculateDispersion(List<Double> g, double average) {
        double disperison = 0.0;
        for(Double f : g) {
            double upperTerm = Math.pow((f - average), 2);
            double bottomTerm = g.size() - 1;
            disperison += upperTerm / bottomTerm;
        }

        return disperison;
    }

    private static double calculateAverage(List<Double> G) {
        double summation = 0.0;

        for(Double f : G) {
            summation += f;
        }

        return summation / G.size();
    }
}
